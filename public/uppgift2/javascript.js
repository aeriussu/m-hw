// We can modify the below values to add buttons to our navigation.
// Increase "navLinks = 4" to any other number and add a name you'd
// like to be displayed for that entry in "navLinkNames" and the 
// file it should load into "navLinkUrls"
var navLinks = 4;
var navLinkNames = ["Home", "Projects", "About Me", "More 'Keets"];
var navLinkUrls = ["home.html", "projects.html", "aboutme.html", "keets.html"];

// Our handle for our XMLHttpRequest
var client;


// When the site is finished loading we'll start a chain reaction 
// of function calls that will set up the navigation, load the 
// default page (depending on whether there's an anchor value 
// present or not) and sets one of the corresponding navigation 
// buttons to look active by adding the "active" class to the link
window.onload = function(){
    
    setupNav();
}  

// Sets up the navigation by inserting an unordered list and with 
// x amount of list items containing links that are filled with 
// values that correspond with the variables at the top of this 
// javascript file
function setupNav(){
    var nav = document.getElementsByTagName("nav");

    var navHTML = "<ul>\n";

    for(var i = 0; i < navLinks; i++){
        navHTML += "<li>\n";
        navHTML += "<a href='#" + (i + 1) + "' id='navLink" + (i + 1) + "' onClick=\"loadUrl('" + navLinkUrls[i] + "', " + (i+1) + ");\"'>";
        navHTML += navLinkNames[i];
        navHTML += "</a>\n";
        navHTML += "</li>\n";
        
    }
    navHTML += "</ul>\n";

    // Insert the navigation
    nav[0].innerHTML = navHTML;

    // Set the default URL to load to home, or whatever is in our anchor
    // default name: "Home"
    // default file: "home.html" (loaded from the "content" folder)
    var defaultPage = "";
    if(window.location.hash){
        // Grab the anchor and remove the "#" to grab our
        // navigation position
        defaultPage = window.location.hash.substring(1);;
    }
    else{
        // If no anchor is detected, we'll set it to load the default, the
        // first value in our array.
        defaultPage = 1;
    }

    // Arrays start at 0, so we have to send the defaultPage value minus 1
    loadUrl(navLinkUrls[(defaultPage - 1)], defaultPage);
}

// Javascript nav link
function setActiveNavButton(navLinkPosition){
    // Clear all the classes from the navigation
    document.getElementById("navLink1").className = "";
    document.getElementById("navLink2").className = "";
    document.getElementById("navLink3").className = "";
    document.getElementById("navLink4").className = "";

    // Add the 'active' class to the link we clicked on to change the active
    // button
    document.getElementById("navLink" + navLinkPosition).classList.add("active");
}

// Load the URL and set the corresponding button in the navigation as active
function loadUrl(receivedUrl, navLinkPosition){
    // We're about to make a new request
    client = new XMLHttpRequest();
    
    // When the handler is ready for it, run the loadContent function
    client.onreadystatechange = loadContent;

    // We specify through what method the content will be received, and 
    // which file will be retrieved
    client.open("GET", "content/" + receivedUrl);
    client.send(null);

    setActiveNavButton(navLinkPosition);
}

// Load the content into the container
function loadContent(){
    document.getElementById("contentContainer").innerHTML = client.responseText;
}